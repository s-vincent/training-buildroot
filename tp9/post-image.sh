#!/bin/bash

set -e

BOARD_DIR="$(dirname $0)"
BOARD_NAME="$(basename ${BOARD_DIR})"
GENIMAGE_CFG="${BOARD_DIR}/genimage-${BOARD_NAME}.cfg"
GENIMAGE_TMP="${BUILD_DIR}/genimage.tmp"

# Pass an empty rootpath. genimage makes a full copy of the given rootpath to
# ${GENIMAGE_TMP}/root so passing TARGET_DIR would be a waste of time and disk
# space. We don't rely on genimage to build the rootfs image, just to insert a
# pre-built one in the disk image.

trap 'rm -rf "${ROOTPATH_TMP}"' EXIT
ROOTPATH_TMP="$(mktemp -d)"

for arg in "$@"
do
  case "${arg}" in
    --serial)
    # enable UART otherwise no serial output!
    if ! grep -qE "^enable_uart=1" ${BINARIES_DIR}/rpi-firmware/config.txt
    then
      cat << __EOF__ >> "${BINARIES_DIR}/rpi-firmware/config.txt"
enable_uart=1
__EOF__
    fi
    ;;
    --u-boot)
    GENIMAGE_CFG="${BOARD_DIR}/genimage-${BOARD_NAME}-uboot.cfg"
    sed -e '/^kernel=/s,=.*,=u-boot.bin,' -i "${BINARIES_DIR}/rpi-firmware/config.txt"
    ;;
    --rt)
    if ! grep -qE "dwc_otg.fiq_enable=0" "${BINARIES_DIR}/rpi-firmware/cmdline.txt"
    then
      echo "Adding dwc_otg options for PREEMPT-RT and Xenomai"
      sed -i.bak "s/^/dwc_otg.fiq_enable=0 dwc_otg.fiq_fsm_enable=0 dwc_otg.nak_holdoff=0 /" \
        ${BINARIES_DIR}/rpi-firmware/cmdline.txt
    fi
    ;;
    --xenomai)
    if ! grep -qE "isolcpus=0,1" ${BINARIES_DIR}/rpi-firmware/cmdline.txt
    then
      echo "Adding isolcpus=0,1 xenomai.suported_cpus=0x3 option for Xenomai"
      sed -i.bak "s/^/isolcpus=0,1 xenomai.supported_cpus=0x3 /" \
        ${BINARIES_DIR}/rpi-firmware/cmdline.txt
    fi
    ;;
  esac
done

rm -rf "${GENIMAGE_TMP}"

genimage \
	--rootpath "${ROOTPATH_TMP}"   \
	--tmppath "${GENIMAGE_TMP}"    \
	--inputpath "${BINARIES_DIR}"  \
	--outputpath "${BINARIES_DIR}" \
	--config "${GENIMAGE_CFG}"

exit $?
