#!/bin/sh

# ensure docker is up and running
systemctl start docker
sleep 5

# launch docker-compose
cd /root/mender-server/production
./run up -d

