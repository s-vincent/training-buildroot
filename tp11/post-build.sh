#!/bin/bash

set -u
set -e

# Add a console on tty1
if [ -e ${TARGET_DIR}/etc/inittab ]; then
    grep -qE '^tty1::' ${TARGET_DIR}/etc/inittab || \
	sed -i '/GENERIC_SERIAL/a\
tty1::respawn:/sbin/getty -L  tty1 0 vt100 # HDMI console' ${TARGET_DIR}/etc/inittab
fi

# adapted from https://github.com/aduskett/buildroot-mender-configs/

mkdir -p "${TARGET_DIR}"/data

function make_data_partition(){
  DATA_PART_SIZE=64M
  # Create the data partition
  rm -rf "${BINARIES_DIR}"/data.ext4
  mkdir -p "${TARGET_DIR}"/data
  fakeroot "${HOST_DIR}"/sbin/mkfs.ext4 \
    -d "${TARGET_DIR}"/data \
    -r 1 \
    -N 0 \
    -m 5 \
    -L "data" \
    -O ^64bit "${BINARIES_DIR}"/data.ext4 "${DATA_PART_SIZE}"
}

OPENSSH=$(grep "BR2_PACKAGE_OPENSSH_SERVER" ${BR2_CONFIG} | cut -d'=' -f2)
SSH_KEY_DIR=/data/etc/ssh
WPA_SUPPLICANT=$(grep "BR2_PACKAGE_WPA_SUPPLICANT=" ${BR2_CONFIG} | cut -d'=' -f2)
BLUEZ=$(grep "BR2_PACKAGE_BLUEZ5_UTILS=" ${BR2_CONFIG} | cut -d'=' -f2)

# If OpenSSH is selected, we should change the ssh key directory to a place
# that won't be overwritten with every new update.
# Idealy, on a system with an eMMC, for security purposes these keys should
# live in a seperate partition that isn't wiped with a factory reset
# (IE: /factory or /persistent).However, in the case of the RPi, there is no
# eMMC, so /data/ is fine.

function sshd_fixup(){

  if [ "${OPENSSH}" = "y" ]; then
    SSHD_CONFIG=${TARGET_DIR}/etc/ssh/sshd_config
    mkdir -p ${TARGET_DIR}/data/etc/ssh
    sed "/AuthorizedKeysFile/c\AuthorizedKeysFile ${SSH_KEY_DIR}/authorized_keys" -i ${SSHD_CONFIG}
    sed "/ssh_host_rsa_key/c\HostKey ${SSH_KEY_DIR}/ssh_host_rsa_key" -i ${SSHD_CONFIG}
    sed "/ssh_host_ed25519_key/c\HostKey ${SSH_KEY_DIR}/ssh_host_ed25519_key" -i ${SSHD_CONFIG}
    sed "/ssh_host_ecdsa_key/c\HostKey ${SSH_KEY_DIR}/ssh_host_ecdsa_key" -i ${SSHD_CONFIG}
    if ! grep -qE "\-f /data" "${TARGET_DIR}/lib/systemd/system/sshd.service"; then
      sed "s/ssh-keygen -A/ssh-keygen -A -f \/data/" -i ${TARGET_DIR}/lib/systemd/system/sshd.service
    fi
  fi
}

function wpa_supplicant_fixup(){
  if [ "${WPA_SUPPLICANT}" = "y" ]; then
    mkdir -p "${TARGET_DIR}"/data/etc
    if [[ ! -L ${TARGET_DIR}/etc/wpa_supplicant.conf ]]; then
      cp ${TARGET_DIR}/etc/wpa_supplicant.conf ${TARGET_DIR}/data/etc/wpa_supplicant.conf
      rm -f ${TARGET_DIR}/etc/wpa_supplicant.conf
      ln -sf /data/etc/wpa_supplicant.conf ${TARGET_DIR}/etc/wpa_supplicant.conf
    fi
  fi
}

function bluetooth_fixup(){
  if [ "${BLUEZ}" = "y" ]; then
    mkdir -p ${TARGET_DIR}/data/bluetooth
    if [[ ! -L ${TARGET_DIR}/var/lib/bluetooth ]]; then
      rm -rf ${TARGET_DIR}/var/lib/bluetooth
      ln -sf /data/bluetooth ${TARGET_DIR}/var/lib/bluetooth
    fi
    sed -i 's/^StateDirectory=bluetooth/#StateDirectory=bluetooth/' ${TARGET_DIR}/lib/systemd/system/bluetooth.service
    sed -i 's/^ConfigurationDirectory=bluetooth/#ConfigurationDirectory=bluetooth/' ${TARGET_DIR}/lib/systemd/system/bluetooth.service
    sed -i 's/^ProtectSystem=strict/ProtectSystem=full/' ${TARGET_DIR}/lib/systemd/system/bluetooth.service
  fi
}

sshd_fixup
wpa_supplicant_fixup
bluetooth_fixup

MENDER=$(grep "BR2_PACKAGE_MENDER=" ${BR2_CONFIG} | cut -d'=' -f2)
function mender_fixup(){
	if [ "${MENDER}" = "y" ]; then
		BOARD_DIR="$(dirname $0)"
		BOARD_NAME="$(basename ${BOARD_DIR})"
		BUILD_VERSION=$(cat ${BOARD_DIR}/RELEASE.txt)

		echo "device_type=${BOARD_NAME}" > "${TARGET_DIR}"/etc/mender/device_type
		echo "artifact_name=${BUILD_VERSION}" > "${TARGET_DIR}"/etc/mender/artifact_info

		mkdir -p ${TARGET_DIR}/data/mender
		mkdir -p ${TARGET_DIR}/data/u-boot

		# The mender package creates var/lib/mender directory as a symlink to
		# /var/run/mender which is link to /run. However, /run is often times mounted
		# as a tmpfs, which makes the mender folder disapear. Instead, remove the
		# symlink and recreate it as a directory, then symlink data to
		# the new /var/lib/mender directory.

		rm -rf ${TARGET_DIR}/var/lib/mender
		ln -sf /data/mender ${TARGET_DIR}/var/lib/mender

		if [[ ! -L ${TARGET_DIR}/etc/fw_env.config ]]; then
			cp ${TARGET_DIR}/etc/fw_env.config ${TARGET_DIR}/data/u-boot/
			rm -f ${TARGET_DIR}/etc/fw_env.config
			ln -sf /data/u-boot/fw_env.config ${TARGET_DIR}/etc/fw_env.config
		fi
	fi
}

mender_fixup
make_data_partition

